#!/bin/bash
dpath=${1%/}

if test -z $dpath; then
    >&2 echo "Error: Must provide a deployment path"
    exit 1
fi

echo "attempting to make the directory: $dpath"
mkdir -p $dpath

echo "deploying at ... $dpath"
echo 

# venv_path=$VIRTUAL_ENV
#
# if [ "$OS" == "Windows_NT" ]; then
#     venv_path=$(cygpath $VIRTUAL_ENV)
# fi


function deploy_submodule() {
    sm_dpath="$(dirname "$1/$sm_path")"

    echo "deploying submodule $name to $sm_dpath ..."
    mkdir -p "$sm_dpath"
    echo cp -R $toplevel/$sm_path $1/$sm_path
    rm -rf $1/$sm_path
    cp -R $toplevel/$sm_path $1/$sm_path
    rm $1/$sm_path/.git
    return 0
}

export -f deploy_submodule


myfiles=(\
  nebula.pth
  nebula/__init__.py
  nebula/applications/__init__.py
  nebula/common/__init__.py
)

echo
echo copying submodules ...
echo

git submodule foreach "deploy_submodule $dpath"

echo
echo copying submodules done !
echo
echo copying main files ...
echo DEBUG = False > nebula/__init__.py
echo

for i in "${myfiles[@]}"
do
    echo "copying $i to $dpath/$i ..."
    cp $i $dpath/$i
done

echo DEBUG = True > nebula/__init__.py

ppath=$dpath
if [ "$OS" == "Windows_NT" ]; then
    ppath=$(cygpath -w $dpath)
fi


echo Generating UserSetup.mel
python $dpath/mayaapps/loader/client/generate_usersetup.py -d


rm -r $dpath/userSetup.mel || true
echo Copying to deployment root
cp $dpath/mayaapps/loader/client/userSetup.mel $dpath/
