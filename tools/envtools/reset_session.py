#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Reset Session by deleting all or particular modules custom modules in sys.path

Code adapted from:

    https://medium.com/@nicholasRodgers/sidestepping-pythons-reload-function-without-restarting-maya-2448bab9476e

"""

from __future__ import print_function
import inspect
from os.path import dirname 
import sys
import importlib

def reset_session():
    # Basically, kill all modules that aren't the init_ modules
    if globals().has_key('init_modules'):
        for m in [x for x in sys.modules.keys() if x not in init_modules]:
            del (sys.modules[m])
    else:
        init_modules = sys.modules.keys()


def reset_session_for_path(userPath=None):
    if userPath is None:
        userPath = dirname(__file__)
    # Convert this to lower just for a clean comparison later  
    userPath = userPath.lower()

    toDelete = []
    # Iterate over all the modules that are currently loaded
    for key, module in sys.modules.iteritems():
        try:
            moduleFilePath = inspect.getfile(module).lower()
            
            try:
                if moduleFilePath == __file__.lower():
                    continue
            except NameError:
                pass
            
            if moduleFilePath.startswith(userPath):
                toDelete.append(key)

        except Exception as e: #  make it quiet
            pass
    
    
    print(toDelete)
    for module in toDelete:
        print ("Removing %s" % module)
        del (sys.modules[module])


def reset_session_for_module(module='nebula'):
    module = importlib.import_module(module)
    mod_path = module.__file__
    mod_path = dirname(mod_path)
    print ('Removing all loaded modules from the given path', mod_path)
    reset_session_for_path(mod_path)


#########################################
        
if __name__ == '__main__':
    reset_session_for_module()
    reset_session_for_module('matteIds')
