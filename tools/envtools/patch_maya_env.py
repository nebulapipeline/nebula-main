# -*- coding: utf-8 -*-

import os
import argparse
import logging
import re
import shutil
import sys


logging.basicConfig(level=logging.DEBUG)


default_maya_version = '2018'
logger = logging.getLogger('patch_maya_env')
curdir = os.path.dirname(__file__)
start_token_re = re.compile('MAYAENV_PATCH_START', re.IGNORECASE)
end_token_re = re.compile('MAYAENV_PATCH_END', re.IGNORECASE)
maya_version_re = re.compile(r'(set MAYA_VERSION=)(\d+)', re.IGNORECASE)


def get_virtual_env():
    return os.environ.get('VIRTUAL_ENV', '')


def check_virtual_env(env):
    return os.path.isfile(os.path.join(env, 'Scripts', 'activate.bat')) and \
        os.path.isdir(os.path.join(env, 'Lib', 'site-packages'))


def find_project_root(pattern='.git'):
    comps = os.path.abspath(curdir).split(os.path.sep)
    for n in range(len(comps)):
        path = os.path.sep.join(comps[:n+1])
        if path.endswith(':'):
            path += os.path.sep
        if pattern in os.listdir(path) and \
                os.path.isdir(os.path.join(path, pattern)):
            return path


def get_patch(path, maya_version=default_maya_version):
    with open(path) as _file:
        if maya_version is default_maya_version:
            return _file.readlines()
        else:
            lines = []
            for line in _file.readlines():
                line = maya_version_re.sub(lambda m: m.group(1) + maya_version,
                                           line)
                lines.append(line)
            return lines


def patch_file(path,
               patch_file,
               maya_version=default_maya_version,
               remove_only=False):
    
    logger.info('patching {}'.format(path))

    if not os.path.isfile(path):
        logger.error('{} is not a valid file'.format(path))
        sys.exit(1)

    with open(path, 'r+') as _file:
        in_patch = False
        lines = []
        for line in _file.readlines():
            if in_patch:
                if end_token_re.search(line):
                    in_patch = False
            elif start_token_re.search(line):
                in_patch = True
            else:
                lines.append(line)
        _file.seek(0)
        _file.writelines(lines)
        _file.truncate()

        if not remove_only:
            patch = get_patch(patch_file, maya_version=maya_version)
            _file.writelines(patch)

    return path


def patch_maya_env(
        maya_version=default_maya_version, env=None, remove=False):
    if env is None:
        env = get_virtual_env()

    if not check_virtual_env(env):
        logger.error('{} is not a valid virtual env'.format(env))
        sys.exit(1)

    # patch activate script
    patch_file(os.path.join(env, 'Scripts', 'activate.bat'),
               os.path.join(curdir, 'activate_ext.bat'),
               maya_version=maya_version,
               remove_only=remove)

    # patch deactivate script
    patch_file(os.path.join(env, 'Scripts', 'deactivate.bat'),
               os.path.join(curdir, 'deactivate_ext.bat'),
               maya_version=maya_version,
               remove_only=remove)

    sitec_dst = os.path.join(env, 'Lib', 'site-packages', 'sitecustomize.py')
    if remove:
        if os.path.isfile(sitec_dst):
            logger.info('Removing {}'.format(sitec_dst))
            os.unlink(sitec_dst)
    else:
        sitec_src = os.path.join(curdir, 'sitecustomize.py')
        logger.info('Adding {}'.format(sitec_dst))
        shutil.copy(sitec_src, sitec_dst)
        with open(sitec_dst, 'a') as _file:
            _file.write("site.addsitedir(r'{}')".format(find_project_root()))


def print_args(*args, **kwargs):
    print(args, kwargs)


def main():
    parser = argparse.ArgumentParser(
        description='Patch provided virtual env to work with maya',
        epilog='''Patches activate and deactivate scripts of virtual env to be
        able to run with maya''')

    parser.add_argument('--virtual-env', '-e', default=None,
                        help="path of virtual env to patch ")

    parser.add_argument('--maya-version',
                        '-m',
                        default=default_maya_version,
                        help="Version of maya to be patched")

    parser.add_argument('--remove', '-r', default=False,
                        action='store_true', help='remove virtual env patch')

    namespace = parser.parse_args()

    patch_maya_env(
        maya_version=namespace.maya_version,
        env=namespace.virtual_env,
        remove=namespace.remove)


if __name__ == '__main__':
    main()
