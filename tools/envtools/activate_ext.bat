REM MAYAENV_PATCH_START

if defined _OLD_PYTHON_PATH (
    set "PYTHONPATH=%_OLD_PYTHON_PATH%;%cd%"
) else (
    if not defined PYTHONPATH goto ENDIFPPATH
        set "_OLD_PYTHON_PATH=%PYTHONPATH%"
        set "PYTHONPATH=%PYTHONPATH%;%cd%"
    REM ) else (
    :ENDIFPPATH
    if defined PYTHONPATH goto ENDIFNOTPPATH
        set "PYTHONPATH=%cd%"
    :ENDIFNOTPPATH
    REM )
)

set MAYA_VERSION=2018
set "MAYA_LOCATION=c:\Program Files\Autodesk\Maya%MAYA_VERSION%"
set "PATH=%MAYA_LOCATION%\bin;%PATH%"
FOR /F "tokens=3 delims= " %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G)
set MAYA_APP_DIR=%docsdir%\maya

REM MAYAENV_PATCH_END
