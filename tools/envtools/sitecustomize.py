from __future__ import print_function

import os
import site

_maya_ver = os.environ.get("MAYA_VERSION", '2018')
_m_loc = os.environ.get("MAYA_LOCATION", '')
_app = os.environ.get("MAYA_APP_DIR", '')

os.environ["PYTHONHOME"] = (_m_loc+"/Python")
os.environ["Path"] = _m_loc+"/bin;" + os.environ["Path"]
os.environ["QT_PLUGIN_PATH"] = _m_loc+"/qt-plugins"

for ppath in os.environ.get('PYTHONPATH', '').split(os.pathsep):
    site.addsitedir(ppath)

site.addsitedir(_app+"/scripts")
site.addsitedir(_app+"/"+_maya_ver+"/scripts")
site.addsitedir(_m_loc+"/bin/python27.zip")
site.addsitedir(_m_loc+"/Python/DLLs")
site.addsitedir(_m_loc+"/Python/lib")
site.addsitedir(_m_loc+"/Python/lib/plat-win")
site.addsitedir(_m_loc+"/Python/lib/lib-tk")
site.addsitedir(_m_loc+"/bin")
site.addsitedir(_m_loc+"/Python")
site.addsitedir(_m_loc+"/Python/Lib/site-packages")
site.addsitedir(_m_loc+"/devkit/other/pymel/extras/completion/py")
site.addsitedir(os.path.abspath('.'))
site.addsitedir('.')
