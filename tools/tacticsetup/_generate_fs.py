#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
import random
import os


def make_file(path):
    with open(path, 'w+'):
        pass


def generate_fs(
        path,
        seed=0,
        r_seqs=(4, 8), r_shots=(5, 20),
        handles=(10, 10), sh_length=(150, 400)):
    random.seed(seed)

    print('generating random data ... for %s' % path)

    seq_main = os.path.join(path, 'SEQUENCES')
    os.mkdir(seq_main)

    in_handle, out_handle = handles
    frame = 0
    for seq_index in range(1, random.randint(*r_seqs) + 1):
        seq_code = 'SQ%03d' % (seq_index)
        seq_dir = os.path.join(seq_main, seq_code)
        os.mkdir(seq_dir)
        shots_main = os.path.join(seq_dir, 'SHOTS')
        os.mkdir(shots_main)

        for sh_index in range(1, random.randint(*r_shots) + 1):
            sh_code = '%s_SH%03d' % (seq_code, sh_index)
            sh_dir = os.path.join(shots_main, sh_code)
            os.mkdir(sh_dir)
            length = random.randint(*sh_length)
            frame += length
            start = frame - in_handle
            end = frame + out_handle + length

            anim_dir = os.path.join(sh_dir, 'animatic')
            os.mkdir(anim_dir)

            for image_nr in range(start, end):
                image_name = '%s.%04d.jpg' % (sh_code, image_nr)
                image_path = os.path.join(anim_dir, image_name)
                print('Creating file %s' % image_path)
                make_file(image_path)


if __name__ == '__main__':
    import sys
    print(sys.argv[1])
    generate_fs(sys.argv[1])
