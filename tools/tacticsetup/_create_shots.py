from __future__ import print_function

import random
import nebula.auth.user as user

sk = '__search_key__'

user.login('admin', 'aa')

server = user.get_server()


s_nouns = ["A dude", "My mom", "The king", "Some guy", "A cat with rabies",
           "A sloth", "Your homie", "This cool guy my gardener met yesterday",
           "Superman"]
p_nouns = ["These dudes", "Both of my moms", "All the kings of the world",
           "Some guys", "All of a cattery's cats",
           "The multitude of sloths living under your bed", "Your homies",
           "Like, these, like, all these people", "Supermen"]
s_verbs = ["eats", "kicks", "gives", "treats", "meets with", "creates",
           "hacks", "configures", "spies on", "retards", "meows on",
           "flees from", "tries to automate", "explodes"]
p_verbs = ["eat", "kick", "give", "treat", "meet with", "create", "hack",
           "configure", "spy on", "retard", "meow on", "flee from",
           "try to automate", "explode"]
infinitives = ["to make a pie.", "for no apparent reason.",
               "because the sky is green.", "for a disease.",
               "to be able to make toast explode.",
               "to know more about archeology."]


def sen_maker():
    '''Makes a random senctence from the different parts of speech'''
    if random.random() < 0.5:
        return ' '.join([
            random.choice(s_nouns),
            random.choice(s_verbs),
            random.choice(s_nouns + p_nouns).lower(),
            random.choice(infinitives)])
    else:
        return ' '.join([
            random.choice(p_nouns),
            random.choice(p_verbs),
            random.choice(s_nouns + p_nouns).lower(),
            random.choice(infinitives)])


def generate_shots(
        project, seed=0, template='episodic',
        r_eps=(1, 3), r_seqs=(4, 8), r_shots=(5, 20),
        handles=(10, 10), sh_length=(150, 400)):
    random.seed(seed)

    episodes = []
    sequences = []
    shots = []

    server.set_project(project['code'])

    print('generating random data ... for %s' % project['code'])

    if template != 'episodic':
        r_eps = (1, 1)

    in_handle, out_handle = handles
    for ep_index in range(1, random.randint(*r_eps) + 1):
        ep_code = 'EP%02d' % ep_index
        if template != 'episodic':
            ep_code = 'default'
        episodes.append({
            'code': ep_code,
            'sort_order': ep_index * 10,
            })
        frame = 0
        for seq_index in range(1, random.randint(*r_seqs) + 1):
            seq_code = '%s_SQ%03d' % (ep_code, seq_index)
            if template != 'episodic':
                seq_code = 'SQ%03d' % (seq_index)
            sequences.append({
                'code': seq_code,
                'episode_code': ep_code,
                'sort_order': seq_index * 10,
                'description': sen_maker()
                })
            for sh_index in range(1, random.randint(*r_shots) + 1):
                sh_code = '%s_SH%03d' % (seq_code, sh_index)
                length = random.randint(*sh_length)
                shots.append({
                    'code': sh_code,
                    'sequence_code': seq_code,
                    'sort_order': sh_index * 10,
                    'frame_in': frame,
                    'frame_out': frame + length,
                    'tc_frame_start': frame - in_handle,
                    'tc_frame_end': frame + out_handle + length,
                    'description': sen_maker()
                    })
                frame += length

    print('inserting %d episodes ... for %s' % (len(episodes),
          project['code']))
    server.insert_multiple('vfx/episode', episodes)

    print('inserting %d sequences ... for %s' % (
        len(sequences), project['code']))
    server.insert_multiple('vfx/sequence', sequences)

    print('inserting %d shots ... for %s' % (
        len(shots), project['code']))
    server.insert_multiple('vfx/shot', shots)


print('getting projects ...')
bilal = server.query('sthpw/project', filters=[('code', 'bilal')], single=True)
usman = server.query('sthpw/project', filters=[('code', 'usman')], single=True)
tt = server.query('sthpw/project', filters=[('code', 'trendy_three')],
                  single=True)
generate_shots(bilal)
generate_shots(tt, seed=5, r_seqs=(2, 4), r_shots=(2, 6))
generate_shots(usman, seed=10, template='commercial',
               r_seqs=(2, 2), r_shots=(4, 10))
