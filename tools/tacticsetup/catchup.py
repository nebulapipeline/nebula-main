#!python
# -*- coding: utf-8 -*-
'''
Find maya files on the NAS Server and check them into tactic server
'''

import os
import logging
import sys
import csv
import re
import hashlib
from typing import List, Dict, Union, Optional, Any  # noqa: F401
import site

site.addsitedir(r'd:/talha.ahmed/Workspace/nebulapipeline/libs/')

import tactic_client_lib as tcl  # noqa: E402

logger = logging.getLogger('catchup')
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.INFO)

ASSET_COMPS = ['model', 'rig', 'shaded']
CATS = ['character', 'environment', 'vegetation', 'props']
SKIP = ['xgen', 'renderData', 'images']

SERVER_NAME = 'ice-tactic'
LOGIN = '******'
PASSWORD = '******'
PROJECT = 'alrajhi_education'
BASEDIR = r'N:\AlRajhi_Education'

SERVER = None  # type: Optional[tcl.TacticServerStub]


def explore_assets_dir(assets_dir):
    logger.debug('Exploring Assets Directory: {0}'.format(assets_dir))
    assets = []
    for cat in CATS:
        cat_dir = os.path.join(assets_dir, cat)
        if os.path.isdir(cat_dir):
            assets.extend(explore_cat(assets_dir, cat) or [])
    return assets


def explore_cat(assets_dir, cat):
    assets = []
    cat_path = os.path.join(assets_dir, cat)
    logger.info('Exploring Category Directory: %s ' % cat_path)
    for name in os.listdir(cat_path):
        if name in SKIP:
            continue
        path = os.path.join(cat_path, name)
        if os.path.isdir(path):
            if check_asset(path):
                assets.append({
                    'code': name.lower().strip(),
                    'asset_category': cat.replace('\\', '/'),
                    'name': make_asset_name(name.lower().strip()),
                    'path': path
                })
            else:
                assets.extend(explore_cat(assets_dir, os.path.join(cat, name)))
    return assets


def check_asset(assets_path):
    dir_list = os.listdir(assets_path)
    if any([comp in dir_list for comp in ASSET_COMPS]):
        return True


def make_asset_name(code):
    return ' '.join([word.capitalize() for word in code.split('_')])


def write_assets(assets, path):
    with open(path, 'w') as csvfile:
        writer = csv.DictWriter(
            csvfile,
            fieldnames=['code', 'name', 'asset_category', 'path'],
            dialect='excel')
        writer.writeheader()
        for asset in assets:
            writer.writerow(asset)
    logger.info("Written %d assets to %s" % (len(assets), path))


def find_asset_maya_files(asset):
    maya_files = {}
    for comp in ASSET_COMPS:
        comp_re = r'%s_%s_v(\d{3}).m[ab]' % (asset['code'], comp)
        comp_dir = os.path.join(asset['path'], comp)
        if not os.path.isdir(comp_dir):
            continue
        for _file in os.listdir(comp_dir):
            match = re.match(comp_re, _file, re.IGNORECASE)
            if match:
                path = os.path.join(comp_dir, _file)
                maya_files[path] = {
                    'path': path,
                    'asset': asset['code'],
                    'asset_category': asset['asset_category'],
                    'context': comp,
                    'version': int(match.group(1))
                }
    return maya_files


def get_all_asset_maya_files(assets):
    maya_files = {}
    for asset in assets:
        maya_files.update(find_asset_maya_files(asset))
    return maya_files


def write_maya_files(maya_files, maya_files_csv):
    with open(maya_files_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile,
                                fieldnames=[
                                    'path', 'asset', 'asset_category',
                                    'context', 'version'
                                ],
                                dialect='excel')
        writer.writeheader()
        for maya_file in maya_files.values():
            writer.writerow(maya_file)
    logger.info("Written %d maya_files to %s" %
                (len(maya_files), maya_files_csv))


def get_maya_files(directory):
    forbidden = ['recover_assets', ]
    maya_files = []
    for root, _dirs, _files in os.walk(directory):
        for _dir in _dirs[:]:
            if any(
                (re.match(word, _dir, re.IGNORECASE)
                    for word in forbidden)):  # noqa: E125
                _dirs.remove(_dir)
        for _file in _files:
            _, ext = os.path.splitext(_file)
            if ext.lower() in ['.ma', '.mb']:
                maya_files.append(os.path.join(root, _file))
    return maya_files


def write_file_list(_files, path):
    with open(path, 'w') as csvfile:
        writer = csv.writer(csvfile, dialect='excel')
        for _file in _files:
            writer.writerow([_file])
    logger.info("Written %d files to %s" % (len(_files), path))


def login(server_name, username, password, project=None):
    """ Login Server """
    global SERVER

    SERVER = tcl.TacticServerStub(setup=False)
    SERVER.set_server(server_name)
    ticket = SERVER.get_ticket(username, password)
    SERVER.set_ticket(ticket)

    if project is not None:
        SERVER.set_project(project)

    return SERVER


def ensure_server(func):
    def _wrapper(*args, **kwargs):
        if SERVER is None:
            login(SERVER_NAME, LOGIN, PASSWORD, PROJECT)
        func(*args, **kwargs)

    _wrapper.__doc__ = func.__doc__
    return _wrapper


@ensure_server
def update_assets(assets_data):
    ''' Update Assets from the data from the disk '''

    assets_from_server = SERVER.query('vfx/asset')  # type: ignore
    assets_from_server = {
        asset.get('__search_key__'): asset
        for asset in assets_from_server
    }

    insert_data = []
    update_data = dict()

    for asset in assets_data:
        sk = SERVER.build_search_key(  # type: ignore
            'vfx/asset',
            code=asset['code'],
            project_code=SERVER.get_project())  # type: ignore
        asset = asset.copy()
        asset.pop('path')
        if sk in assets_from_server:
            update_data[sk] = asset
        else:
            insert_data.append(asset)

    if insert_data:
        logger.info('Inserting %d assets' % len(insert_data))
        SERVER.insert_multiple('vfx/asset', insert_data,
                               triggers=True)  # type: ignore

    if update_data:
        logger.info('Updating %d assets' % len(update_data))
        SERVER.update_multiple(update_data, triggers=True)  # type: ignore


def md5sum(filename, blocksize=65536):
    hash = hashlib.md5()
    with open(filename, "rb") as f:
        for block in iter(lambda: f.read(blocksize), b""):
            hash.update(block)
    return hash.hexdigest()


@ensure_server
def create_checkins(asset_maya_files):
    SERVER.start('Doing Inplace Checkins for %s files' %
                 len(asset_maya_files))  # type: ignore
    logger.info('Doing Inplace Checkins for %s files' % len(asset_maya_files))
    snapshots = []
    try:
        for path, maya_file in asset_maya_files.items():
            sk = SERVER.build_search_key(  # type: ignore
                'vfx/asset',
                code=maya_file['asset'],
                project_code=SERVER.get_project()  # type: ignore
            )
            ss = SERVER.get_snapshot(  # type: ignore
                sk,
                maya_file['context'],
                version=maya_file['version'])
            if not ss:
                ss = SERVER.simple_checkin(  # type: ignore
                    sk,
                    maya_file['context'],
                    path,
                    version=maya_file['version'],
                    mode='inplace')
                logger.info("Added %s to %s" % (maya_file['path'], ss['code']))
            snapshots.append(ss)

    except Exception as exc:
        SERVER.abort()  # type: ignore
        raise exc

    SERVER.finish("%d maya files added" %
                  len(asset_maya_files))  # type: ignore
    logger.info("%d maya files added" % len(asset_maya_files))

    return snapshots


@ensure_server
def fix_inplace(base_dir=BASEDIR):
    file_objs = SERVER.query(
        'sthpw/file',  # type: ignore
        filters=[('project_code', SERVER.get_project())])  # type: ignore
    for fobj in file_objs:
        update = {}

        if not fobj['relative_dir']:
            checkin_dir = fobj['checkin_dir']
            relative_dir = os.path.join(
                SERVER.get_project(),  # type: ignore
                os.path.relpath(checkin_dir, base_dir))
            update['relative_dir'] = relative_dir

        if not fobj['md5']:
            try:
                update['md5'] = md5sum(fobj['source_path'])
            except FileNotFoundError:  # noqa: F821
                logger.error('File Not Found {}'.format(fobj['source_path']))

        if update:
            logger.info('Updating fileinfo for %s' % fobj['source_path'])
            SERVER.update(fobj['__search_key__'], update)  # type: ignore


def main():
    assets_dir = r'N:\AlRajhi_Education\assets'

    assets_found = explore_assets_dir(assets_dir)
    assets_csv = os.path.join(
        'd:\\',
        os.path.basename(os.path.dirname(assets_dir)) + '_assets.csv')
    write_assets(assets_found, assets_csv)

    maya_files = get_all_asset_maya_files(assets_found)
    maya_files_csv = os.path.join(
        'd:\\',
        os.path.basename(os.path.dirname(assets_dir)) +
        '_asset_maya_files.csv')
    write_maya_files(maya_files, maya_files_csv)

    unassociated_files = set(get_maya_files(assets_dir)) - \
        set(maya_files.keys())
    bad_files_csv = os.path.join(
        'd:\\',
        os.path.basename(os.path.dirname(assets_dir)) + '_unknown_maya_files.csv')
    write_file_list(unassociated_files, bad_files_csv)

    update_assets(assets_found)
    create_checkins(maya_files)
    fix_inplace()


if __name__ == '__main__':
    main()
