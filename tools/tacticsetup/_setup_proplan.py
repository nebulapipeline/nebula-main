import nebula.common.core as nc
import nebula.backends as be

import sys
from Qt.QtWidgets import QApplication

nc.BackendRegistry.register(be.tactic_backend)
nc.BackendRegistry.set('tactic')

app = QApplication(sys.argv)
