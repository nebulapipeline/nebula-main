from __future__ import print_function

import os
import nebula.auth.user as user


sk = '__search_key__'
user.login('admin', 'aa')

server = user.get_server()

_dir = os.path.dirname(__file__)
assets_dir = os.path.join(_dir, 'assets')

asset_categories = os.listdir(assets_dir)

server.set_project('bilal')

for cat in asset_categories:
    server.insert('vfx/asset_category', {'code': cat})
    print('Inserting Asset Category ', cat)
    cat_dir = os.path.join(assets_dir, cat)

    for asset_code in os.listdir(cat_dir):

        asset_data = {}
        asset_data['code'] = asset_code
        asset_data['asset_category'] = cat
        asset_data['name'] = asset_code.capitalize()
        asset_data['pipeline_code'] = 'vfx/asset'
        print('Creating asset ...', asset_code)
        current_asset = server.insert('vfx/asset', asset_data)

        asset_dir = os.path.join(cat_dir, asset_code)

        for context in os.listdir(asset_dir):
            context_dir = os.path.join(asset_dir, context)

            context_files = os.listdir(context_dir)
            context_files.sort()

            for _file in context_files:
                path = os.path.join(context_dir, _file)
                print("Checking in ...", path)
                server.simple_checkin(current_asset[sk], context, path)
