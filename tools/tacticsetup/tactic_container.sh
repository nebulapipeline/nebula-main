#!/bin/bash

IMAGE_NAME=lazyleopard/nebula-tactic:bare

if test -z $DATA_DIR_BASE; then
    DATA_DIR_BASE=tactic_test_data
fi

if test -z $MAP_DRIVE; then
    MAP_DRIVE=T:
fi

if test -z $TACTIC_PORT; then
    TACTIC_PORT=8000
fi

if test -z $SSH_PORT; then
    SSH_PORT=2222
fi

if test -z $ROOT_PASSWORD; then
    ROOT_PASSWORD="aa"
fi

MAIN_DIR=/tmp
SEP=/
if [ "$OS" == "Windows_NT" ]; then
    echo Working on windows ...
    SEP=\\
    MAIN_DIR=$USERPROFILE
fi

dir_join () {
    local res=$1
    for var in ${@:2}; do
        res=$res$SEP$var;
    done
    echo $res;
}

winsudo () {
    echo winsudo $* ...
    powershell.exe -Command "Start-Process cmd \"/c $*\" -Verb RunAs"
    sleep 3
}

ROOT_DIR=$(dir_join "$MAIN_DIR" $DATA_DIR_BASE )
SANDBOX_DIR=$(dir_join $ROOT_DIR sandbox)
ASSETS_DIR=$(dir_join $ROOT_DIR assets)
PROJECTS_DIR=$(dir_join $ROOT_DIR projects)
HANDOFF_DIR=$(dir_join $ROOT_DIR handoff)
CONFIG_DIR=$(dir_join $ROOT_DIR tactic_config)

echo "ROOT_DIR=$ROOT_DIR"


create_shortcuts () {
    # making sure parent folders exist
    mkdir -p $ASSETS_DIR/unc
    mkdir -p $ASSETS_DIR/map
    mkdir -p $ASSETS_DIR/lnk
    mkdir -p $ASSETS_DIR/clnk

    local proj_code=$1
    local proj_base=$2

    echo Creating project link directories for $proj_code $proj_base...
    local proj_dir=$(dir_join $PROJECTS_DIR $proj_base)

    mkdir -p $proj_dir
    echo This is the main directory for project $proj_code $proj_base > $proj_dir/proj_description

    relpath=$(realpath $proj_dir --relative-to=$ASSETS_DIR/clnk)
    fullpath=$(dir_join $ASSETS_DIR clnk $proj_code)
    echo Creating project links ...

    if [ "$OS" == "Windows_NT" ]; then
        cmd "/c mklink /D $(dir_join $ASSETS_DIR unc $proj_code) ..\\..\\projects\\$proj_code"
        cmd "/c mklink /D $(dir_join $ASSETS_DIR map $proj_code) ..\\..\\projects\\$proj_code"
        cmd "/c docker exec -it tactic ln -s $relpath /home/apache/assets/assets/lnk/$proj_code"
        cmd "/c docker exec -it tactic ls -al /home/apache/assets/assets/lnk/$proj_code/"
    else
        docker exec -it tactic ln -s $relpath /home/apache/assets/assets/lnk/$proj_code
        docker exec -it tactic ls -al /home/apache/assets/assets/lnk/$proj_code/
    fi
    echo ln -s $relpath $fullpath
    ln -s $relpath $fullpath
}

extract_assets () {
    echo Extracting to projects folder
    if [ "$OS" == "Windows_NT" ]; then
        tar -xvzf .env/projects.tar.gz -C $(cygpath $PROJECTS_DIR)
    else
        tar -xvzf .env/projects.tar.gz -C $PROJECTS_DIR
    fi
}

archive_assets () {
    echo Compressing projects folder ...
    if [ "$OS" == "Windows_NT" ]; then
        tar -zcvf .env/projects.tar.gz -C $(cygpath $PROJECTS_DIR) .
    else
        tar -zcvf .env/projects.tar.gz -C $PROJECTS_DIR .
    fi
}

run () {

    echo Creating directories and links ...
    mkdir -p $ROOT_DIR
    if [ "$OS" == "Windows_NT" ]; then
        cmd "/c icacls $ROOT_DIR /inheritance:e /grant everyone:(OI)(CI)F"
    else
        chmod -R a+rws $ROOT_DIR
        setfacl -R -d -m g::rwx -m u::rwx -m o::rwx $ROOT_DIR
    fi

    mkdir -p $SANDBOX_DIR
    mkdir -p $ASSETS_DIR
    mkdir -p $PROJECTS_DIR
    mkdir -p $HANDOFF_DIR
    mkdir -p $CONFIG_DIR

    if [ "$OS" == "Windows_NT" ]; then
        winsudo net share $DATA_DIR_BASE=$ROOT_DIR /grant:everyone,FULL
        echo net use T: /delete
        cmd "/c net use T: /delete"
        echo net use T: $(dir_join \\\\localhost $DATA_DIR_BASE)
        cmd "/c net use T: $(dir_join \\\\localhost $DATA_DIR_BASE)"
    fi

    echo Copying config file ...
    cp .env/tactic_config/* $CONFIG_DIR

    echo Running tactic container ...
    docker run --name tactic \
        --mount type=bind,source=$ROOT_DIR,target=/home/apache/assets \
        --mount type=bind,source=$HANDOFF_DIR,target=/home/apache/handoff \
        --mount type=bind,source=$CONFIG_DIR,target=/home/apache/tactic_data/config \
        -p $TACTIC_PORT:80 -p $SSH_PORT:2222 \
        -e ROOT_PASSWORD=$ROOT_PASSWORD \
        -d lazyleopard/nebula-tactic:bare

    create_shortcuts admin admin
    create_shortcuts trendy_three Trendy_Three
    create_shortcuts usman Usman
    create_shortcuts bilal Bilal

    extract_assets
    if [ "$OS" != "Windows_NT" ]; then
        sudo chown -R apache.apache $ROOT_DIR
        sudo setfacl -R -d -m g::rwx -m u::rwx -m o::rwx $ROOT_DIR
        sudo setfacl -R -d -m g:apache:rwx -m u:apache:rwx $ROOT_DIR
    fi
 }

stop () {
    echo Stopping tactic container ...
    docker container stop tactic;
}

remove () {
    echo removing tactic container ...
    docker container rm tactic;

    if [ "$OS" == "Windows_NT" ]; then
        echo net use T: /delete ...
        cmd "/c net use T: /delete"
        winsudo net share $DATA_DIR_BASE /delete
        echo Removing directories ...
        rm -r $ROOT_DIR
    else
        echo Removing directories ...
        sudo rm -r $ROOT_DIR
    fi

}

start () {
    echo Starting tactic container ...
    docker container start tactic;
}

commit () {
    stop
    archive_assets
    echo Commiting image
    docker commit tactic $IMAGE_NAME
    start
}

push () {
    docker push $IMAGE_NAME
}

restart() {
    stop
    start
}

# Parsing arguments
if [ "$1" = "stop" ]; then
    stop
    if [ "$2" = "--remove" ]; then
        remove
    fi
elif [ "$1" = "remove" ]; then
    remove
elif [ "$1" = "run" ]; then
    run
elif [ "$1" = "start" ]; then
    start
elif [ "$1" = "restart" ]; then
    restart
elif [ "$1" = "commit" ]; then
    commit
elif [ "$1" = "commit" ]; then
    commit
else
    echo Usage: $0 [ACTION] [--remove]
    echo
    echo -e "\tACTION in (run stop remove start restart commit)"
    echo -e "\t--remove is only meaningful when used with the stop action"
    echo
    echo "Caution: May have to use with sudo"
fi
