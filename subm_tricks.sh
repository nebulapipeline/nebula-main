#!/bin/bash

if test -z $local_base_repo; then
    if test -f ./local_base_repo.sh; then
      source ./local_base_repo.sh
    else
      export local_base_repo="/r/Pipe_Repo/Projects/nebula/repos"
    fi
    echo "setting local_base_repo=$local_base_repo"
fi

if test -z $sync_branch; then
    # get the current branch of the base directory

    export sync_branch=$(git rev-parse --abbrev-ref HEAD)
    echo "setting sync_branch=$sync_branch"
fi

print_vars () {
    echo $name $sm_path $displaypath $sha1 $toplevel
}

move_in() {
    # This func replaces the .git file placed in the submodules with its
    # git directory placed in the main modules' .git dir. This will convert
    # the submodule structure to a directory structure with separate git
    # repositories if you rename the base .git dir

    gitfile="$toplevel/$displaypath/.git"
    gitdir="$toplevel/.git/modules/$name"
    moduledir="$toplevel/$displaypath"
    if test -f $gitfile; then
        echo "removing $gitfile"
        rm $gitfile
        echo "copy in $gitdir"
        cp -R $gitdir $gitfile
        echo "removing $gitdir"
        rm -rf $gitdir
    fi
}

move_back() {
    # This function replaces

    gitfile="$toplevel/$displaypath/.git"
    gitdir="$toplevel/.git/modules/$name"
    moduledir="$toplevel/$displaypath"
    if test -d $gitfile; then
        relpath=$(realpath --relative-to $moduledir $gitdir)
        echo "copy back to $gitdir"
        cp -R $gitfile $gitdir
        echo "removing $gitfile"
        rm -rf $gitfile
        echo "gitdir: $relpath" > $gitfile
        print_gitfile
    fi
}

print_gitfile () {
    # print the contents of the git file in the submodules base directory

    gitfile="$toplevel/$displaypath/.git"
    if test -f $gitfile; then
        cat $gitfile
        echo
    fi
}

add_local () {
    remote="$local_base_repo/$name"
    echo "adding $remote as a remote"
    git remote add local $remote
}

remove_local () {
    git remote remove local
}

create_local () {
    remote="$local_base_repo/$name"
    echo "creating $remote as a local bare"
    git clone --bare . "$remote" || true
}

update_remote() {
    # In the case of two sets of repos ... one online and one locally stored
    # This function tries to push all the commits of the current branch
    # fetched from the local repo to the global repo

    local sha=$sha1
    git checkout "$sync_branch"
    git pull local "$sync_branch"
    git pull origin "$sync_branch"
    git push origin "$sync_branch"
    git checkout "$sha"
}

update_local() {
    # In the case of two sets of repos ... one online and one locally stored
    # This fuction tries to push all the commits fetched from the global
    # repository to the local repository

    local sha=$sha1
    git checkout "$sync_branch"
    git pull origin "$sync_branch"
    git push origin "$sync_branch"
    git push local "$sync_branch"
    git checkout "$sha"
}

set_branch() {
    git branch $sync_branch
    git checkout $sync_branch
    git branch --set-upstream-to=origin/$sync_branch
    git config -f $toplevel/.gitmodules submodule.$name.branch $sync_branch
}

OPTIONS=(\
    print_vars print_gitfile\
    move_in move_back\
    add_local remove_local\
    create_local update_remote update_local
    set_branch)

export -f ${OPTIONS[*]}


if [ $# -eq 0 ]; then
    # When no arguments are specified offer the user a menu interface

    select opt in ${OPTIONS[*]}; do
        if test -z $opt; then
            echo bad option
        else
            echo $opt
            git submodule foreach "$opt"
        fi
        exit
    done
else
    echo $1
    git submodule foreach "$1";
fi
